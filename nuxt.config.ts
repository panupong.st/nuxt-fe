export default defineNuxtConfig({
  devtools: { enabled: false },
  css: ['./assets/css/global.css'],
  build: {
    transpile: ['vuetify'],
  },
  modules: ['@nuxtjs/strapi'],
  strapi: {
    url: process.env.STRAPI_URL || "http://localhost:1337",
    prefix: '/api',
    version: 'v4',
    cookie: {},
    cookieName: 'strapi_jwt',
  },
});
