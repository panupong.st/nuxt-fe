import axios from "axios";
export default defineNuxtRouteMiddleware(async (to, _from) => {
    const token = useStrapiToken();
    const url = useStrapiUrl();
    const user = useStrapiUser();
    
    if (!user.value) {
        useCookie("redirect", { path: "/" }).value = to.fullPath;
        return navigateTo("/login");
    }

    const response = await axios.get(`${url}/users/me`, {
        params: {
            populate: {
                role: {
                    fields: {
                        0: "name"
                    }
                }
            },
        },
        headers: {
            Authorization: `Bearer ${token.value}`,
        },
    })

    if (response.data.role.name !== "Admin") {
        useCookie("redirect", {path: "/items-management"}).value = to.fullPath;
        return navigateTo("/");
    }
});