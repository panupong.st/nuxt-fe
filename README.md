# Equipment Borrowing and Returning System for Academic Department | Nuxt3

ชื่อโครงงาน
    ระบบยืม-คืนอุปกรณ์สาขาวิชา
    Equipment Borrowing and Returning System for Academic Department
จัดทำโดย
    นายสุริยา แสนหาญ (suriya.sa@rmuti.ac.th)
    นายภาณุพงศ์ แซ่ตั้ง (panupong.st@rmuti.ac.th)

### `Prerequisites`
    - node.js : v20
    - npm : v6 and above, or yarn (แนะนำ)

### `install`

โคลนแอปพลิเคชัน

```
git clone https://gitlab.com/suriya.sa/nuxt-fe.git
```

เข้าโฟลเดอร์แอปพลิเคชัน

```
cd project-name
```

ติดตั้ง

```
yarn install
# or
npm install
```

### Develop

Start the development server on `http://localhost:3000`
เริ่มแอปพลิเคชัน

```
yarn dev
# or
npm run dev
```



